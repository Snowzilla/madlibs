<newsentence>
	
	<div class='newSentence'>
		<h3>Write a new sentence!</h3>
		<form class='form-inline'>
			<div class='form-group'>
				<input type='text' id="sentence1" placeholder="Begin sentence"></input>
				<label>Noun</label>
				<input type='text' id="sentence2" placeholder="More sentence"></input>
				<label>Adjective</label>
				<input type='text' id="sentence3" placeholder="More sentence"></input>
				<label>Noun</label>
				<input type='text' id="sentence4" placeholder="End of sentence"></input>
			</div>
			
		</form>
	</div>
	<button onclick={ addItem }>Create a New Sentence</button>
	

	<script>
		this.addItem = function(event) {
			noun1 = " noun ";
			adjective1 = " adjective ";
			noun2=" noun ";
			sent1=$('#sentence1').val();
			sent2=$('#sentence2').val();
			sent3=$('#sentence3').val();
			sent4=$('#sentence4').val();
			$('#sentence').html(sent1 + "<b>"+noun1+"</b>"+sent2 +"<b>"+adjective1+"</b>"+" "+sent3+"<b>"+noun2+"</b>"+sent4);	
		};
	</script>

	<style>
		.newSentence {
			background-color: orange;
		}
	</style>
</newsentence>