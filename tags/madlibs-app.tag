<madlibs-app>

	<div class="madlibsapp">
		<button onclick={ toggleShow }>Toggle Show Sentence</button>
		<h3>Hello  { opts.user.first } { user.last }! </h3>


<!-- 		<input id="ketchup" name="special" type="text">
		<button onclick={ getInput }>IDs and Name attributes available like this.id this.name</button> -->

		<form>
			<div class="form-group">
				<label for="todoInput">Enter words below</label>
				<input id="noun1" type="text" class="form-control" placeholder="noun" >
				<input id="adjective1" type="text" class="form-control" placeholder="adjective" >
				<input id="noun2" type="text" class="form-control" placeholder="noun" >
			</div>
		</form>
		<button onclick={ addItem }>Let's see your Madlib!</button>

		<h3>Your sentence is:</h3>
		<h4 id='sentence' show={ x }>Hello, you __(noun)___! You look like a ___(adjective)___ ___(noun)____.</h4>

	</div>

	<script>

		this.user = this.opts.user;
		this.wordList = this.opts.wordList

		this.x = true;

		this.toggleShow = function(event) {
			this.x = !this.x;
		};


		this.addItem = function(event) {
			noun1=$('#noun1').val();
			adjective1 = $('#adjective1').val();
			noun2=$('#noun2').val();
			$('#sentence').html(sent1+" " + "<b>"+noun1+"</b>"+" "+sent2 +"<b>"+" "+adjective1+"</b>"+" "+sent3+"<b>"+ " " +noun2+"</b>"+ " "+sent4);
		};

		this.on('update', function() {
			console.log('parent update', this);
		});



		// this.getInput = function(event) {
		// 	console.log(this.ketchup.value);
		// };

	</script>

	<style>
		.madlibsapp {
			background-color: yellow;
			padding: 30px;
		}
	</style>
</madlibs-app>
